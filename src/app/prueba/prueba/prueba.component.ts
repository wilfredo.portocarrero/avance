import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit {
  id$: Observable<string>;
  id:string;

  constructor(private route: ActivatedRoute) { 
   
  }

  ngOnInit() {
    //this.id$ = this.route.paramMap.pipe(map(paramMap => paramMap.get('id')));

    this.id= this.route.snapshot.paramMap.get('id');
    console.log(this.id);
  }

}
