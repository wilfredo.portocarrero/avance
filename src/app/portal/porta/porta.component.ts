import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/proyectos/_servicios/token.service';


@Component({
  selector: 'app-porta',
  templateUrl: './porta.component.html',
  styleUrls: ['./porta.component.css']
})
export class PortaComponent implements OnInit {
  mensaje: number = 1;
  proyectos: any = [];
  token: any;
  tokencli:any;


  constructor(protected tokenservice: TokenService) {
    this.proyectos = {
      "proyectos": [
        {
          "id": 1,
          "foto": "https://images.unsplash.com/photo-1520769669658-f07657f5a307?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
          "titulo": "Google API",
          "link":"gmap"
        },
        {
          "id": 2,
          "foto": "https://images.unsplash.com/photo-1520769669658-f07657f5a307?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
          "titulo": "Carousel",
          "link":"carousel"
        },
        {
          "id": 3,
          "foto": "https://images.unsplash.com/photo-1520769669658-f07657f5a307?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
          "titulo": "intereses",
          "link":"intereses"
        },
        {
          "id": 4,
          "foto": "https://images.unsplash.com/photo-1520769669658-f07657f5a307?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
          "titulo": "validaciones",
          "link":"validaciones"
        }
      ]
    }

    
  }

  ngOnInit() {
    console.log(this.proyectos);
    this.Token();
    this.TokenclienteFuncion();
  }

  //funcion token
  Token() {
    this.tokenservice.getToken().subscribe(
      (data) => {

        this.token = data['sToken'];
        localStorage.setItem('token', this.token);
       
        //alert(localStorage.getItem('token'));
      },
      (error) => {
        console.log(error);
      }

    )
 
  }

  TokenclienteFuncion(){
    this.tokenservice.getClientToken().subscribe(
      (data) => {
        
        this.tokencli = data['sToken'];
        localStorage.setItem('tokencli', this.tokencli);
       // console.log('ESTE TU TOKEN CLIENTE ',this.tokencli);
       
        //alert(localStorage.getItem('token'));
      },
      (error) => {
        console.log(error);
      }
  
    )
  }

}
