import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortalRoutingModule } from './portal-routing.module';
import { PortaComponent } from './porta/porta.component';
import { HttpClientModule } from '@angular/common/http';
import { TokenService } from '../proyectos/_servicios/token.service';


@NgModule({
  declarations: [PortaComponent],
  imports: [
    CommonModule,
    PortalRoutingModule,
    HttpClientModule
  ],
  providers: [TokenService],
})
export class PortalModule { }
