import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { GmapService } from '../_servicios/gmap.service';
import { MapsAPILoader, MouseEvent } from '@agm/core';

@Component({
  selector: 'app-gmap',
  templateUrl: './gmap.component.html',
  styleUrls: ['./gmap.component.css']
})
export class GmapComponent implements OnInit {
  texto: string = "google maps";
  
  zoom: number;
  token: string;
  valortoken: string;
  @ViewChild('search', { static: false })
  public searchElementRef: ElementRef;
  geoCoder: google.maps.Geocoder;
  latitude: number;
  longitude: number;
  address: string;
  establecimiento: string;
  autocomplete: string;


  constructor(protected gmapservice: GmapService, private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }

  ngOnInit() {


    this.Establecimiento();

    //post token extraer latitud y long de establecimiento
    this.establecimiento = localStorage.getItem('establecimiento');

    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"],
        componentRestrictions: { country: "pe" }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();

          this.longitude = place.geometry.location.lng();
          
          this.zoom = 18;
          this.getAddress(this.latitude, this.longitude);
        });
      });
    });







  }

   //funciones de inicio
   Establecimiento() {
    this.gmapservice.getestablecimiento().subscribe(
      (data) => {
        //this.latEst=data['sLatitud'];
        localStorage.setItem('latitud', data['objDto']['sLatitud']);
        localStorage.setItem('longitud', data['objDto']['sLongitud']);
        localStorage.setItem('establecimiento', data['objDto']['sNombre']);

      }
    )
  }

  //google maps funciones


  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        var latitud_establecimiento = parseFloat(localStorage.getItem('latitud'));
        var longitud_establecimiento = parseFloat(localStorage.getItem('longitud'));
        this.latitude = latitud_establecimiento;
        this.longitude = longitud_establecimiento;
        this.zoom = 18;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }


  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
     // console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 18;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }


 


}
