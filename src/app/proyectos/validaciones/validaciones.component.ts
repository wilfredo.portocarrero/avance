import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-validaciones',
  templateUrl: './validaciones.component.html',
  styleUrls: ['./validaciones.component.css']
})
export class ValidacionesComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  myForm: FormGroup;
  date = new Date();
  prefTelefono: any;
  //validadores
  reactTelefono: any = [Validators.required, Validators.pattern("(^(?!.*([9][8][7][6][5][4][3][2][1])).*)(^[0-9]{9,9}$)"),
  Validators.pattern("^(\\d)(?!\\1+$)\\d{8}$"), Validators.maxLength(9)];

  reactTelefonoInt: any = [Validators.required, Validators.pattern("^[0-9]*$")];

  reactDocumento: any = [Validators.required, Validators.pattern("(^(?!.*([1][2][3][4][5][6][7][8])).*)(^[0-9]{8,8}$)"),
  Validators.pattern("^(\\d)(?!\\1+$)\\d{7}$"), Validators.maxLength(8)];

  reactDocumentoPASSCE: any = [Validators.required, Validators.pattern("^\\s*([0-9a-zA-Z]+)\\s*$"), Validators.pattern("^\\S*$"), Validators.maxLength(12)];



  

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      // fecha: [''],
      documento: ['', this.reactDocumento],
      telefono: ['', this.reactTelefono],
    });


    //datepicker validar si es mayor de edad
    this.date.setFullYear(this.date.getFullYear() - 18);
   
  }

  get f() { return this.registerForm.controls; }



  telefono(val) {
    if (val == 0) {
      this.prefTelefono = "0";
      this.PrefTelefonoDef();

    } else if (val == 1) {
      this.prefTelefono = "9";
      this.PrefTelefonoDef();
    } else if (val == 2) {
      this.prefTelefono = "";
      this.PrefTelefonoInt();
    }
  }

  documento(val) {
    if (val == 0) {

      this.PrefDocDNI();

    } else if (val == 1 || val == 2) {

      this.PrefDocPASSCE();

    }
  }


  //validacion de documentos
  PrefDocDNI() {
    this.registerForm.controls.documento.clearValidators();
    this.registerForm.controls.documento.setValidators(this.reactDocumento);
  }
  PrefDocPASSCE() {
    this.registerForm.controls.documento.clearValidators();
    this.registerForm.controls.documento.setValidators(this.reactDocumentoPASSCE);
  }

  //validacion de telefonos
  PrefTelefonoDef() {
    this.registerForm.controls.telefono.clearValidators();
    this.registerForm.controls.telefono.setValidators(this.reactTelefono);
  }

  PrefTelefonoInt() {
    this.registerForm.controls.telefono.clearValidators();
    this.registerForm.controls.telefono.setValidators(this.reactTelefonoInt);
  }



  //otros

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

}
