import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CarouselService {
  token : string;

 

  constructor(private http : HttpClient) { }

  getfotos() {

    var headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
    }
    return this.http.get("https://us-central1-devthepoint-47564.cloudfunctions.net/Home?Carrusel=true", headers);

  }
  fotos() {
    this.getfotos().subscribe(
      (data) => {
        //Guardar como objeto en localstorage
        localStorage.setItem('fotosCarousel',JSON.stringify(data['listObjsDto']));
      }
    )
  }
}
