import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  token: string;
  latEst: string;
  private headers = new HttpHeaders({ "Content-Type": "application/json", "Authorization": " Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImEwYjQwY2NjYmQ0OWQxNmVkMjg2MGRiNzIyNmQ3NDZiNmZhZmRmYzAiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZGV2dGhlcG9pbnQtNDc1NjQiLCJhdWQiOiJkZXZ0aGVwb2ludC00NzU2NCIsImF1dGhfdGltZSI6MTU3MTg0NjU2OSwidXNlcl9pZCI6IkVEcFo0a280S2tkY2lsaDRHNzl3eXFvMTBvOTMiLCJzdWIiOiJFRHBaNGtvNEtrZGNpbGg0Rzc5d3lxbzEwbzkzIiwiaWF0IjoxNTcxODQ2NTY5LCJleHAiOjE1NzE4NTAxNjksImVtYWlsIjoidG9ubnlyYW1vQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInRvbm55cmFtb0BnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.XnUh9_pMadeDmigtipNPmAtjBFwi6Vt4gYEWGjikX6GR-UMs3fOra3qlLptG4_uAnoBLV90sxXdtfw-hE0NIk8pT4bK3UovQaXGqGzqfnxNEClL-g4RUcxlAbjeFPzg8V_2h_7Q3N1MfN0hjKxX0Mv0vANAvsPJbpjbg06GF_gKxNt8YSp4vsrTFTdhA4HaK7ODQaGbs9BkCkMFeAu7wy6oGMte_M3bgwiJpBPX0g0pC-5yF1jPxVb4UQ_RnUufboOZJIGw0VWnP2HGHmp-txr_DyEeppUBjbPmZVXCsLU_6j3_j_3cr4EQabNIBg_RQprmWwJEiZr26_N0Yie391w" });

  constructor(protected http: HttpClient) { }

  getToken() {
    return this.http.get('https://us-central1-devthepoint-47564.cloudfunctions.net/security-key');
  }

  getClientToken() {
    var headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
    }
    const data = { 'sEmail': 'wilfredo.portocarrero@materiagris.pe', 'sPassword': '123456' };
    console.log(localStorage.getItem('token'));
    return this.http.post("https://us-central1-devthepoint-47564.cloudfunctions.net/login-inWithEmail", data, headers);

  }




}
