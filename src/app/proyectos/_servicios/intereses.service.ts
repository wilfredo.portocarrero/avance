import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InteresesService {

  constructor(protected http: HttpClient) { }

  getIntereses() {

    var headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${localStorage.getItem('tokencli')}`)
    }

    
    console.log(localStorage.getItem('token'));
    return this.http.get("https://us-central1-devthepoint-47564.cloudfunctions.net/listPreferences", headers);

  }

  envIntereses(pref) {

    var headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${localStorage.getItem('tokencli')}`)
    }
    const data = {'aPreferencias': pref};

    
   
    return this.http.post("https://us-central1-devthepoint-47564.cloudfunctions.net/updatePreferences",data, headers);

  }
}
