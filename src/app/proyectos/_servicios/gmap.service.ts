import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GmapService {

  constructor(protected http: HttpClient) { }


  getestablecimiento() {

    var headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
    }
   
    return this.http.get("https://us-central1-devthepoint-47564.cloudfunctions.net/Place?sIdEstablecimiento=2iuVr966gGh9Y1FJLoyk", headers);

  }

}
