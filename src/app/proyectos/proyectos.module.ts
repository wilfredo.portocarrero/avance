import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ProyectosRoutingModule } from './proyectos-routing.module';
import { GmapComponent } from './gmap/gmap.component';
import { CarouselComponent } from './carousel/carousel.component';
import { InteresesComponent } from './intereses/intereses.component';
import { ValidacionesComponent } from './validaciones/validaciones.component';
import { TokenService } from './_servicios/token.service';
import { CarouselService } from './_servicios/carousel.service';
import { HttpClientModule } from '@angular/common/http'; 
import { GmapService } from './_servicios/gmap.service';
import { AgmCoreModule } from '@agm/core';
import { InteresesService } from './_servicios/intereses.service';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [GmapComponent, CarouselComponent, InteresesComponent, ValidacionesComponent],
  imports: [
    CommonModule,
    ProyectosRoutingModule,
    BsDatepickerModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyCA1m8-w56s4sKrb8AIkqF5vL8Oc8ST8go',
      libraries: ['geometry', 'places']
   }),
  ],
  providers: [TokenService,CarouselService,GmapService,InteresesService],
})
export class ProyectosModule { }
