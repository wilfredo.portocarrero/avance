import { Component, OnInit } from '@angular/core';
import { TokenService } from '../_servicios/token.service';
import { InteresesService } from '../_servicios/intereses.service';

@Component({
  selector: 'app-intereses',
  templateUrl: './intereses.component.html',
  styleUrls: ['./intereses.component.css']
})
export class InteresesComponent implements OnInit {
  array3: any[] = [];
  preferenciasUsuario: any[] = [];
  seleccion = {};
  

  constructor(protected serviceintereses: InteresesService) { }

  ngOnInit() {
     //listado de intereses
     this.preferenciasUsuario = JSON.parse(localStorage.getItem('preferenciaUsuario'));
  }



    //funcion para quitar elemento de arreglo
    removeItemFromArr(arr, item) {
      var i = arr.indexOf(item);
  
      if (i !== -1) {
        arr.splice(i, 1);
      }
    }

    //llena o vacia el arreglo
    llenado(id, index) {
      
      this.seleccion[id]= !this.seleccion[id];
      //this.selection = !this.selection;
     
      if (this.array3.includes(id)) {
        this.removeItemFromArr(this.array3, id);
        console.log(this.array3);
       
      } else {
        this.array3.push(id);
        console.log(this.array3);
      }
  
  
    }

  ListarPreferencia(){

    this.serviceintereses.getIntereses().subscribe(
      (data) => {
        console.log('ESTE TU PREFERENCIA CLIENTE ',data);
        console.log('TUS PREFERENCIAS',data['listObjsDto']['listPreferencias']);
        localStorage.setItem('preferenciaUsuario',JSON.stringify(data['listObjsDto']['listPreferencias']));
      },
      (error) => {
        console.log(error);
      }
    )
  }

    //envia arreglo al servicio
    enviarInteres() {
      this.enviarPreferencia(this.array3);
    }
  
    //funcion para usar el servicio
    enviarPreferencia(pref) {
      this.serviceintereses.envIntereses(pref).subscribe(
        (data) => {
          console.log('este es tu resultado de envio pref ', data);
  
        },
        (error) => {
          console.log(error);
        }
      )
    }
 

}
