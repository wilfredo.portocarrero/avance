import { Component, OnInit } from '@angular/core';

import { CarouselService } from '../_servicios/carousel.service';
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  token: any;
  fotosCarousel:any[]=[];
  constructor(protected carouselservice : CarouselService) { }

  ngOnInit() {
   
    this.Fotos();
    //convertir objeto
    this.fotosCarousel =JSON.parse(localStorage.getItem('fotosCarousel'));
    
  }







  //funciones de inicio


  Fotos(){
    this.carouselservice.getfotos().subscribe(
      (data) => {
        localStorage.setItem('fotosCarousel',JSON.stringify(data['listObjsDto']));
      }
    )
  }

}
