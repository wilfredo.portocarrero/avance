import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GmapComponent } from './gmap/gmap.component';
import { CarouselComponent } from './carousel/carousel.component';
import { InteresesComponent } from './intereses/intereses.component';
import { ValidacionesComponent } from './validaciones/validaciones.component';


const routes: Routes = [{
  path:'gmap',
  component: GmapComponent
},
{
  path:'carousel',
  component: CarouselComponent
},
{
  path:'intereses',
  component: InteresesComponent
},
{
  path:'validaciones',
  component: ValidacionesComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProyectosRoutingModule { }
