import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './core/not-found/not-found.component';


const routes: Routes = [
 /* {
    path: '',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },*/
  {
    path: '',
    loadChildren: () => import('./portal/portal.module').then(m => m.PortalModule)
  },
  {
    path: 'prueba',
    loadChildren: () => import('./prueba/prueba.module').then(m => m.PruebaModule)
  },
  {
    path: 'proyectos',
    loadChildren: () => import('./proyectos/proyectos.module').then(m => m.ProyectosModule)
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
